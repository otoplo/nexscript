import {
  ContractArtifact, SourceArtifact, asmToScript, hash160, optimiseBytecode, prepareMcpContract, scriptToBytecode,
} from '@nexscript/utils';
import { ANTLRInputStream, CommonTokenStream } from 'antlr4ts';
import fs, { PathLike } from 'fs';
import { utf8ToBin } from '@bitauth/libauth';
import { generateArtifact } from './artifact/Artifact.js';
import { Ast, ContractNode, SourceFileNode } from './ast/AST.js';
import AstBuilder from './ast/AstBuilder.js';
import ThrowingErrorListener from './ast/ThrowingErrorListener.js';
import GenerateTargetTraversal from './generation/GenerateTargetTraversal.js';
import { CashScriptLexer } from './grammar/CashScriptLexer.js';
import { CashScriptParser } from './grammar/CashScriptParser.js';
import SymbolTableTraversal from './semantic/SymbolTableTraversal.js';
import TypeCheckTraversal from './semantic/TypeCheckTraversal.js';
import EnsureFinalRequireTraversal from './semantic/EnsureFinalRequireTraversal.js';
import { version } from './index.js';
import DependencyTraversal from './semantic/DependencyTraversal.js';
import ContractAccessorTraversal from './semantic/ContractAccessorTraversal.js';

export function compileString(source: string): SourceArtifact {
  // Lexing + parsing
  let topLevelAst = parseCode(source);

  const artifacts: { [contractName: string]: ContractArtifact } = {};
  const replacements: any = {};

  const prepareArtifacts = (ast: SourceFileNode | ContractNode): SourceFileNode | ContractNode => {
    const dependencyTraversal = new DependencyTraversal();
    ast = ast.accept(dependencyTraversal) as ContractNode;

    const compileContract = (contractName: string): void => {
      const dependencies = dependencyTraversal.buildDependencyList(contractName, dependencyTraversal.dependencyMap);
      for (const dependency of dependencies) {
        if (!artifacts[dependency]) {
          compileContract(dependency);
        }
      }

      if (artifacts[contractName]) {
        return;
      }

      const contractIndex = ast.contracts.findIndex((contract) => contract.name === contractName);

      // MCP artifact
      if (ast.contracts[contractIndex].contracts.length) {
        ast.contracts[contractIndex] = prepareArtifacts(ast.contracts[contractIndex]) as ContractNode;

        const mcpDependencies = ast.contracts[contractIndex].contracts
          .map((val) => artifacts[val.name].dependencies || []).flat()
          .filter((value, index, array) => array.indexOf(value) === index);

        const mcpArtifact = getMcpArtifact(ast.contracts[contractIndex] as ContractNode, artifacts);
        mcpArtifact.dependencies = mcpDependencies;

        const mcpContract = prepareMcpContract(
          mcpArtifact.contracts[0].contractName,
          mcpArtifact.contracts[0].abi[0].name,
          mcpArtifact,
        );
        const optimisedBytecode = asmToScript(mcpContract.artifact.bytecode);

        artifacts[contractName] = mcpArtifact;
        replacements[`${contractName}.template`] = scriptToBytecode(optimisedBytecode);
        replacements[`${contractName}.templateHash`] = hash160(scriptToBytecode(optimisedBytecode));
        replacements[`${contractName}.constraintHash`] = utf8ToBin(`${contractName}.constraintHash`);
        replacements[`${contractName}.lockingBytecode`] = utf8ToBin(`${contractName}.lockingBytecode`);

        return;
      }

      // Semantic analysis
      ast.contracts[contractIndex] = ast.contracts[contractIndex].accept(
        new ContractAccessorTraversal(replacements),
      ) as ContractNode;
      ast.contracts[contractIndex] = ast.contracts[contractIndex].accept(new SymbolTableTraversal()) as ContractNode;
      ast.contracts[contractIndex] = ast.contracts[contractIndex].accept(new TypeCheckTraversal()) as ContractNode;
      ast.contracts[contractIndex] = ast.contracts[contractIndex].accept(new EnsureFinalRequireTraversal()) as ContractNode;

      // Code generation
      const traversal = new GenerateTargetTraversal();
      ast.contracts[contractIndex] = ast.contracts[contractIndex].accept(traversal) as ContractNode;
      const bytecode = traversal.output;

      // Bytecode optimisation
      const optimisedBytecode = optimiseBytecode(bytecode);

      artifacts[contractName] = generateArtifact(ast.contracts[contractIndex], optimisedBytecode, dependencies);
      replacements[`${contractName}.template`] = scriptToBytecode(optimisedBytecode);
      replacements[`${contractName}.templateHash`] = hash160(scriptToBytecode(optimisedBytecode));
      replacements[`${contractName}.constraintHash`] = utf8ToBin(`${contractName}.constraintHash`);
      replacements[`${contractName}.lockingBytecode`] = utf8ToBin(`${contractName}.lockingBytecode`);
    };

    ast.contracts.forEach((contract) => {
      compileContract(contract.name);
    });

    return ast;
  };

  topLevelAst = prepareArtifacts(topLevelAst);
  const contracts = topLevelAst.contracts.map((contract) => artifacts[contract.name]);

  return {
    contracts,
    source,
    compiler: {
      name: 'nexc',
      version,
    },
    updatedAt: new Date().toISOString(),
  };
}

// returns partially assembled McpArtifact
// its bytecode and full abi depends upon mast contract executed
export function getMcpArtifact(node: ContractNode, allArtifacts: { [contractName: string]: ContractArtifact }): ContractArtifact {
  const dependencies: string[] = [];
  const artifact = generateArtifact(node, [Uint8Array.from([])], dependencies);

  artifact.contracts = node.contracts.map((mastContractNode) => {
    const mastContractArtifact = allArtifacts[mastContractNode.name];

    mastContractArtifact.constructorInputs.forEach(() => {
      // the MCPs constraint parameters will be passed to this string as satisfier parameters
      // so we need to skip fetching them from altstack
      mastContractArtifact.bytecode = mastContractArtifact.bytecode.replace(/OP_FROMALTSTACK /, '').trim();
    });

    return mastContractArtifact;
  });

  return artifact;
}

export function compileFile(codeFile: PathLike): SourceArtifact {
  const code = fs.readFileSync(codeFile, { encoding: 'utf-8' });
  return compileString(code);
}

export function parseCode(code: string): Ast {
  // Lexing (throwing on errors)
  const inputStream = new ANTLRInputStream(code);
  const lexer = new CashScriptLexer(inputStream);
  lexer.removeErrorListeners();
  lexer.addErrorListener(ThrowingErrorListener.INSTANCE);
  const tokenStream = new CommonTokenStream(lexer);

  // Parsing (throwing on errors)
  const parser = new CashScriptParser(tokenStream);
  parser.removeErrorListeners();
  parser.addErrorListener(ThrowingErrorListener.INSTANCE);
  const parseTree = parser.sourceFile();

  // AST building
  const ast = new AstBuilder(parseTree).build() as Ast;

  return ast;
}
