import { compileString } from "../../src/compiler";

describe('Generate merkle root macro code', () => {
  it(`should fail to generate merkle root macro for tree height non-integer`, () => {
    const source = `
contract merkleDatabase(bytes20 root, int treeSize) {
  function append(bytes merkleProof, bytes20 lastLeaf) {
    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot("a", merkleProof, treeSize, lastLeaf);
    require(generatedRoot == root); //Validate that the merkle proof is valid.
  }
}`;
    expect(() => compileString(source)).toThrow();
  });

  it(`should fail to generate merkle root macro for tree height -1`, () => {
    const source = `
contract merkleDatabase(bytes20 root, int treeSize) {
  function append(bytes merkleProof, bytes20 lastLeaf) {
    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot(-1, merkleProof, treeSize, lastLeaf);
    require(generatedRoot == root); //Validate that the merkle proof is valid.
  }
}`;
    expect(() => compileString(source)).toThrow();
  });

  it(`should fail to generate merkle root macro for tree height 0`, () => {
    const source = `
contract merkleDatabase(bytes20 root, int treeSize) {
  function append(bytes merkleProof, bytes20 lastLeaf) {
    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot(0, merkleProof, treeSize, lastLeaf);
    require(generatedRoot == root); //Validate that the merkle proof is valid.
  }
}`;
    expect(() => compileString(source)).toThrow();
  });

  it(`should generate merkle root macro for tree height 2`, () => {
    const source = `
contract merkleDatabase(bytes20 root, int treeSize) {
  function append(bytes merkleProof, bytes20 lastLeaf) {
    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot(1, merkleProof, treeSize, lastLeaf);
    require(generatedRoot == root); //Validate that the merkle proof is valid.
  }
}`;
    expect(() => compileString(source)).not.toThrow();
  });
});
