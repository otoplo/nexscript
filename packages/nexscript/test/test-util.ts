import {
  hexToBin,
} from '@bitauth/libauth';
import nexcore from 'nexcore-lib';
import { Output, Network, TokenDetails } from '../src/interfaces.js';
import { network as defaultNetwork } from './fixture/vars.js';
import { scriptToNexaAddress } from '../src/utils.js';

export function getTxOutputs(tx: any, network: Network = defaultNetwork): Output[] {
  return tx.outputs.map((o: any) => {
    const OP_RETURN = '6a';
    const scriptHex = o._scriptBuffer.toString('hex');

    if (scriptHex.startsWith(OP_RETURN)) {
      return { to: hexToBin(scriptHex), amount: 0n };
    }

    const result: Output = {
      amount: BigInt(o.satoshis),
      to: scriptToNexaAddress(o.script, network),
    };

    if (o.script.chunks[0].opcodenum !== nexcore.Opcode.OP_0) {
      const unsignedBN = nexcore.crypto.BN.fromBuffer(o.script.chunks[1].buf, { endian: 'little' });
      const amount = BigInt.asIntN(64, unsignedBN.toBigInt());
      result.token = {
        groupId: o.script.chunks[0].buf.toString('hex'),
        amount,
      } as TokenDetails;
    }

    return result;
  });
}
