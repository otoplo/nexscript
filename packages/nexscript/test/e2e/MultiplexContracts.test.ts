import { SourceArtifact } from '@nexscript/utils';
import { compileString } from '@nexscript/nexc';
import {
  Contract,
  ElectrumNetworkProvider,
} from '../../src/index.js';
import { fund } from '../fixture/vars.js';
import { getContractCreationParamsFromAddress } from '../../src/utils.js';

describe('Multiplex contract tests', () => {
  beforeAll(async () => {
  });

  it('should get contract parameters from address', async () => {
    // constraints, empty visible params
    expect(getContractCreationParamsFromAddress('nexa:nqtsq5g5am5mrk7m99j36jax6vredr3v2qge07gsgggjl40k')).toMatchObject({
      constraintHash: Uint8Array.from([
        238, 233, 177, 219, 219, 41,
        101, 29, 75, 166, 211, 7,
        150, 142, 44, 80, 17, 151,
        249, 16,
      ]),
      visibleArgs: Uint8Array.from([]),
      lockingBytecode: Uint8Array.from([
        0, 81, 20, 238, 233, 177, 219,
        219, 41, 101, 29, 75, 166, 211,
        7, 150, 142, 44, 80, 17, 151,
        249, 16,
      ]),
    });

    // empty constraints, visible params
    expect(getContractCreationParamsFromAddress('nexareg:nqdqq9y45jr9zfnp2qtdewmgwwc58jm8f7th9gsqqy29zxneyykqg')).toMatchObject({
      constraintHash: Uint8Array.from([0x00]),
      visibleArgs: Uint8Array.from([1, 20, 81]),
      lockingBytecode: Uint8Array.from([
        0, 20, 149, 164, 134, 81, 38,
        97, 80, 22, 220, 187, 104, 115,
        177, 67, 203, 103, 79, 151, 114,
        162, 0, 1, 20, 81,
      ]),
    });

    // group address
    expect(getContractCreationParamsFromAddress('nexa:npx9ggzh73kpwekuqzrmypavmcdnxuhfly9333lxwfpx2u6ymnf27esqqqyqpeqt2spqqqqqznkgmyme95ayzec44nhp5m5zm3wtd87e5y2wa6d3m0djjegafwndxpuk3ck9qyvhlygqlgs2j6k9')).toMatchObject({
      constraintHash: Uint8Array.from([
        238, 233, 177, 219, 219, 41,
        101, 29, 75, 166, 211, 7,
        150, 142, 44, 80, 17, 151,
        249, 16]),
      visibleArgs: Uint8Array.from([]),
      lockingBytecode: Uint8Array.from([
        32, 87, 244, 108, 23, 102, 220, 0, 135, 178, 7, 172,
        222, 27, 51, 114, 233, 249, 11, 24, 199, 230, 114, 66,
        101, 115, 68, 220, 210, 175, 102, 0, 0, 8, 0, 228,
        11, 84, 2, 0, 0, 0, 20, 236, 141, 147, 121, 45,
        58, 65, 103, 21, 172, 238, 26, 110, 130, 220, 92, 182,
        159, 217, 161, 20, 238, 233, 177, 219, 219, 41, 101, 29,
        75, 166, 211, 7, 150, 142, 44, 80, 17, 151, 249, 16,
      ]),
    });
  });

  it('should fail to accept an artifact with empty contract list', async () => {
    const provider = new ElectrumNetworkProvider();

    const artifact: SourceArtifact = {
      contracts: [],
      source: '',
    } as any;

    expect(() => new Contract(artifact, [], { provider })).toThrow('Malformed artifact: empty contract list');
  });

  it('should compile and instantiate a multiplex contract', async () => {
    // given
    const code = `
    contract A(string vA, int x) {
      function A() {
        // string a = "asdf";
        // require(a.length == 4);

        bytes constraintHash = B.constraintHash;
        require(constraintHash.length != 0);

        // bytes template = B.template;
        // require(template.length != 0);

        // bytes20 hash = B.templateHash;
        // require(hash.length != 0);
        require(vA == "A");
        require(x == 1);
      }

      function B() {
        require(vA == "B");
        require(x == 2);
      }
    }

    contract B() {
      function Bv(string vB) {
        require(vB == "B");
      }
    }

    contract C(string vC, int x) {
      function Cv() {
        bytes20 hash = A.templateHash;
        require(hash.length != 0);

        bytes constraintHash = B.constraintHash;
        require(constraintHash.length != 0);

        require(vC == "C");
        require(x == 3);
      }
    }
    `;

    const artifact = compileString(code);

    const provider = new ElectrumNetworkProvider();

    expect(() => new Contract(artifact, [], { provider })).toThrow('No contract creation parameters');
    expect(() => new Contract(artifact, [], { provider, contractName: 'A' })).toThrow('No contract creation parameters');
    expect(() => new Contract(artifact, ['C', 3n], {
      provider,
      contractName: 'C',
      dependencyArgs: {
        A: {
          constructorInputs: artifact.contracts[0].constructorInputs,
          constructorArgs: ['A', 1n],
        },
      },
    })).toThrow("No contract creation parameters provided for dependency contract 'B'");
    expect(() => new Contract(artifact, ['C', 3n], {
      provider,
      contractName: 'C',
      dependencyArgs: {
        B: {
          constraintHash: Uint8Array.from([0x00]),
          visibleArgs: Uint8Array.from([]),
          lockingBytecode: Uint8Array.from([]),
        },
      },
    })).toThrow("No contract creation parameters provided for dependency contract 'A'");
    expect(() => new Contract(artifact, [], { provider, contractName: 'test' })).toThrow('not found in artifact');

    const B = new Contract(artifact, [], { provider, contractName: 'B' });
    expect(() => new Contract(artifact, ['A', 1n], {
      provider,
      contractName: 'A',
      dependencyArgs: {
        B: getContractCreationParamsFromAddress(B.address),
      },
    })).not.toThrow();

    const contract = new Contract(artifact, ['C', 3n], {
      provider,
      contractName: 'C',
      dependencyArgs: {
        A: {
          constructorInputs: artifact.contracts[0].constructorInputs,
          constructorArgs: ['A', 1n],
        },
        B: {
          constraintHash: Uint8Array.from([0x00]),
          visibleArgs: Uint8Array.from([]),
          lockingBytecode: Uint8Array.from([]),
        },
      },
    });

    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .Cv()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });
});
