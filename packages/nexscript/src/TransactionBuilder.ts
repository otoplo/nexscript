import {
  binToHex,
} from '@bitauth/libauth';
import delay from 'delay';
import nexcore from 'nexcore-lib';
import {
  Unlocker,
  Output,
  TransactionDetails,
  UnlockableUtxo,
  Utxo,
  InputOptions,
  isUnlockableUtxo,
} from './interfaces.js';
import { NetworkProvider } from './network/index.js';
import { buildError, createOpReturnOutput } from './utils.js';

export interface TransactionBuilderOptions {
  provider: NetworkProvider;
}

export class TransactionBuilder {
  private provider: NetworkProvider;
  private inputs: UnlockableUtxo[] = [];
  private outputs: Output[] = [];

  private locktime: number;
  private maxFee?: bigint;

  public txid: string;

  constructor(
    options: TransactionBuilderOptions,
  ) {
    this.provider = options.provider;
  }

  addInput(utxo: Utxo, unlocker: Unlocker, options?: InputOptions): this {
    this.inputs.push({ ...utxo, unlocker, options });
    return this;
  }

  addInputs(utxos: Utxo[], unlocker: Unlocker, options?: InputOptions): this;
  addInputs(utxos: UnlockableUtxo[]): this;

  addInputs(utxos: Utxo[] | UnlockableUtxo[], unlocker?: Unlocker, options?: InputOptions): this {
    if (
      (!unlocker && utxos.some((utxo) => !isUnlockableUtxo(utxo)))
      || (unlocker && utxos.some((utxo) => isUnlockableUtxo(utxo)))
    ) {
      throw new Error('Either all UTXOs must have an individual unlocker speciifed, or no UTXOs must have an individual unlocker specified and a shared unlocker must be provided');
    }

    if (!unlocker) {
      this.inputs = this.inputs.concat(utxos as UnlockableUtxo[]);
      return this;
    }

    this.inputs = this.inputs.concat(utxos.map(((utxo) => ({ ...utxo, unlocker, options }))));
    return this;
  }

  addOutput(output: Output): this {
    this.outputs.push(output);
    return this;
  }

  addOutputs(outputs: Output[]): this {
    this.outputs = this.outputs.concat(outputs);
    return this;
  }

  // TODO: allow uint8array for chunks
  addOpReturnOutput(chunks: string[]): this {
    this.outputs.push(createOpReturnOutput(chunks));
    return this;
  }

  setLocktime(locktime: number): this {
    this.locktime = locktime;
    return this;
  }

  setMaxFee(maxFee: bigint): this {
    this.maxFee = maxFee;
    return this;
  }

  private checkMaxFee(): void {
    if (!this.maxFee) return;

    const totalInputAmount = this.inputs.reduce((total, input) => total + input.satoshis, 0n);
    const totalOutputAmount = this.outputs.reduce((total, output) => total + output.amount, 0n);
    const fee = totalInputAmount - totalOutputAmount;

    if (fee > this.maxFee) {
      throw new Error(`Transaction fee of ${fee} is higher than max fee of ${this.maxFee}`);
    }
  }

  build(): string {
    this.checkMaxFee();

    const transaction = new nexcore.Transaction();

    this.outputs.forEach((output) => {
      if (output.to instanceof Uint8Array) {
        transaction.addOutput(new nexcore.Transaction.Output({
          type: 0,
          script: nexcore.Script(binToHex(output.to)),
          satoshis: 0,
        }));
      } else if (output.token) {
        transaction.toGrouped(output.to, Buffer.from(output.token.groupId, 'hex'), output.token.amount);
      } else {
        transaction.to(output.to, Number(output.amount), 1); // p2st
      }
    });

    // first add all inputs to complete transaction layout, then sign them in next loop
    this.inputs.forEach((input) => {
      input.unlocker.addInput({ transaction, input });
    });

    // sequences and locktime must be set before producing signatures
    if (this.locktime) {
      if (this.locktime < 5e8) {
        transaction.lockUntilBlockHeight(this.locktime);
      } else {
        transaction.lockUntilDate(this.locktime);
      }
    }

    const { network } = this.provider;

    this.inputs.forEach((input, inputIndex) => {
      input.unlocker.signInput({
        transaction, inputIndex, network,
      });
    });

    const serialized = transaction.serialize();
    // rostrum issue. save txid for later query
    this.txid = transaction.id;
    return serialized;
  }

  // TODO: see if we can merge with Transaction.ts
  async send(): Promise<TransactionDetails>;
  async send(raw: true): Promise<string>;
  async send(raw?: true): Promise<TransactionDetails | string> {
    const tx = this.build();
    try {
      // rostrum issue. use txid and not the returned idem to get the transaction
      const { txid } = this;
      await this.provider.sendRawTransaction(tx);
      return raw ? await this.getTxDetails(txid, raw) : await this.getTxDetails(txid);
    } catch (e: any) {
      const reason = e.error ?? e.message;
      throw buildError(reason, '');
    }
  }

  // TODO: see if we can merge with Transaction.ts
  private async getTxDetails(txid: string): Promise<TransactionDetails>;
  private async getTxDetails(txid: string, raw: true): Promise<string>;
  private async getTxDetails(txid: string, raw?: true): Promise<TransactionDetails | string> {
    for (let retries = 0; retries < 1200; retries += 1) {
      await delay(500);
      try {
        const hex = await this.provider.getRawTransaction(txid);

        if (raw) return hex;

        const nexcoreTransaction = nexcore.Transaction(hex);
        return { ...nexcoreTransaction, txid, hex };
      } catch (ignored) {
        // ignored
      }
    }

    // Should not happen
    throw new Error('Could not retrieve transaction details for over 10 minutes');
  }
}
