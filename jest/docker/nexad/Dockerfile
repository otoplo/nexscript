FROM debian:bookworm-slim

ARG TARGETPLATFORM

RUN groupadd -r nexa && useradd -r -m -g nexa nexa

RUN set -ex \
  && apt-get update \
  && apt-get install -qq --no-install-recommends ca-certificates dirmngr gosu gpg wget \
  && rm -rf /var/lib/apt/lists/*

ENV NEXA_VERSION 1.3.0.2

# install nexa binaries
RUN set -ex \
  && [ $TARGETPLATFORM = "linux/arm64" ] && platform="arm64" || platform="linux64" \
  && export NEXA_URL=https://www.bitcoinunlimited.info/nexa/${NEXA_VERSION}/nexa-${NEXA_VERSION}-${platform}.tar.gz \
  && [ $TARGETPLATFORM = "linux/arm64" ] && sha256="42d634c555e40fa8caea0469a0859f980a407151a4d41dfcd9f20a63243415db" || sha256="e32e05fd63161f6f1fe717fca789448d2ee48e2017d3d4c6686b4222fe69497e" \
  && export NEXA_SHA256="$sha256" \
  && echo "$TARGETPLATFORM" "$NEXA_SHA256" "$NEXA_URL" \
  && cd /tmp \
  && wget -qO nexa.tar.gz "$NEXA_URL" \
  # && echo "$NEXA_SHA256 nexa.tar.gz" | sha256sum -c - \
  && tar -xzvf nexa.tar.gz -C /usr/local --strip-components=1 --exclude=*-qt \
  && rm -rf /tmp/*

# create data directory
ENV NEXA_DATA /data
RUN mkdir "$NEXA_DATA" \
  && chown -R nexa:nexa "$NEXA_DATA" \
  && ln -sfn "$NEXA_DATA" /home/nexa/.nexa \
  && chown -h nexa:nexa /home/nexa/.nexa
VOLUME /data

COPY docker-entrypoint.sh /entrypoint.sh
RUN chmod 777 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 8332 8333 18332 18333
CMD ["nexad"]