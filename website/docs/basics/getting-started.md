---
title: Getting Started
---

## Installing the NexScript compiler
The command line NexScript compiler `nexc` can be installed from NPM.

```bash
npm install -g @nexscript/nexc
```

## Installing the NexScript SDK
The NexScript SDK can be installed into your project with NPM.

```bash
npm install @nexscript/nexscript
```

:::caution
NexScript only offers a NexScript SDK, but NexScript contracts can be integrated into other languages as well. Because there are no ready-to-use SDKs available for them, this is considered advanced usage, and it is recommended to use the JavaScript SDK.
:::

## Writing your first smart contract
There are some examples available on the [Examples page](/docs/language/examples), that can be used to take inspiration from. Further examples of the JavaScript integration can be found on [GitHub](https://gitlab.com/otoplo/nexscript/tree/master/examples). A simple example is included below.

```solidity
pragma nexscript ^0.1.0;

contract TransferWithTimeout(pubkey sender, pubkey recipient, int timeout) {
    // Allow the recipient to claim their received money
    function transfer(sig recipientSig) {
        require(checkSig(recipientSig, recipient));
    }

    // Allow the sender to reclaim their sent money after the timeout is reached
    function timeout(sig senderSig) {
        require(checkSig(senderSig, sender));
        require(tx.time >= timeout);
    }
}
```

:::tip
Read more about the Nexscript language syntax in the [Language Description](/docs/language/contracts).
:::

## Integrating into JavaScript
While more detailed examples are available on GitHub, we show an integration of the `TransferWithTimeout` contract in a JavaScript project.

After compiling the contract file to an artifact JSON with `nexc`, it can be imported into the NexScript SDK.

```bash
nexc ./transfer_with_timeout.nex --output ./transfer_with_timeout.json
```

```javascript
import { ElectrumNetworkProvider, Contract, SignatureTemplate } from '@nexscript/nexscript';
import { alicePriv, alicePub, bobPriv, bobPub } from './keys.js';
import artifact from './transfer_with_timeout.json';

// Initialise a network provider for network operations
const provider = new ElectrumNetworkProvider('mainnet');

// Instantiate a new TransferWithTimeout contract
const contract = new Contract(artifact, [alicePub, bobPub, 100000n], { provider });

// Call the transfer function with Bob's signature
// i.e. Bob claims the money that Alice has sent him
const transferDetails = await contract.functions
  .transfer(new SignatureTemplate(bobPriv))
  .to('nexa:nqtsq5g537fcf6z85pgwk4my5e5ddmypa2sm47mkzavt6zky', 10000n)
  .send();
console.log(transferDetails);

// Call the timeout function with Alice's signature
// i.e. Alice recovers the money that Bob has not claimed
const timeoutDetails = await contract.functions
  .timeout(new SignatureTemplate(alicePriv))
  .to('nexa:nqtsq5g5wtkt44pfqusjj3wulk2n2pd27lhpzg0m326kcnsj', 10000n)
  .send();
console.log(timeoutDetails);
```

:::tip
Read more about the JavaScript SDK in the [SDK documentation](/docs/sdk/instantiation).
:::
