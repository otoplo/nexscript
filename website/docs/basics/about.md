---
title: What is NexScript?
sidebar_label: About NexScript
---

NexScript is a high-level programming language for smart contracts on Nexa. It offers a strong abstraction layer over Nexa's native virtual machine, Nexa Script. The project is a fork of CashScript for Bitcoin Cash. Its syntax is based on Ethereum's smart contract language Solidity, but its functionality is very different since smart contracts on Nexa differ greatly from smart contracts on Ethereum. 

If you're interested to see what kind of things can be built with NexScript, you can look at the [Examples](/docs/language/examples) provided. If you just want to dive into NexScript, refer to the [Getting Started page](/docs/basics/getting-started) and other pages in the documentation.
